Solid Construction & Design is a locally owned & operated kitchen & bathroom remodeling company. We specialize in full service home remodeling, kitchen & bathroom remodeling, flooring and home additions. We are committed to providing affordable services that are performed to the highest standard.

Address: 5549 Auburn Blvd, Sacramento, CA 95841, USA

Phone: 916-260-2066

Website: https://www.solidconstructiondesign.com
